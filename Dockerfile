FROM node:alpine

ARG PORT

WORKDIR /app/

COPY ./app .
RUN npm install
ENV PORT=${PORT}

EXPOSE ${PORT}

CMD ["npm", "run", "dev"]
